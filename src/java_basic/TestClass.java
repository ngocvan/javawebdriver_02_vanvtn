package java_basic;

public class TestClass {

	public static void main(String[] args) {
		String testString = "Automation Testing Tutorial Online 123456";
		// check the string "Tesing" is in testString or not
		if (testString.contains("Testing"))
			System.out.println("Have \"Testing\" is in \"testString\" string");

		// count the number of character a
		int counta = 0;
		for (int i = 0; i < testString.length(); i++) {
			if (testString.charAt(i) == 'a' || testString.charAt(i) == 'A')
				counta++;
		}
		System.out.println("the number of character \"a\": " + counta);

		// check whether "Automation" is in the first of string testString or
		// not
		if (testString.startsWith("Automation"))
			System.out.println("\"Automation\" is in the first of string");
		else
			System.out.println("\"Automation\" is not in the first of string");

		// check whether "Automation" is in the first of string testString or
		// not
		if (testString.endsWith("Automation"))
			System.out.println("\"Online\" is in the end of string");
		else
			System.out.println("\"Online\" is not in the end of string");

		// Return the index of "Tutorial" in string
		System.out.println("The index of \"Tutorial\" in string: " + testString.indexOf("Tutorial"));

		// Return the string after replace string "Online" by string "Offline"
		System.out.println(
				"the string after replace \"Online\" by \"Offline\": " + testString.replace("Online", "Offline"));

		// Count the number of character is the number in string
		//1:
		int countDigit =0;
		for (int i = 0; i < testString.length(); i++) {
			if(Character.isDigit(testString.charAt(i)))
				countDigit++;
		}
		System.out.println("The number of character is digit: " +countDigit);
		
		//2:
		int countDigit2 =0;
		String pattern = "\\d";
		for (int i = 0; i < testString.length(); i++) {
			if(String.valueOf(testString.charAt(i)).matches(pattern))
				countDigit2++;
		}
		System.out.println("The number of character is digit 2: " +countDigit2);
	}

}
