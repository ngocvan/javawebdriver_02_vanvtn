package selenium_online;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Java_Webdriver_03_BrowserWebElementCommandExercise {
	WebDriver driver;
	WebElement errorMessage;
	int timeOut = 10;
	String mailTxtLocator = "//input[@id='mail']";
	String ageRadioLocator = "//input[@id='under_18']";
	String eduAreaLocator = "//textarea[@id='edu']";
	String jobRole1Locator = "//select[@id='job1']";
	String devChBoxLocator = "//input[@id='development']";
	String slider1Locator = "//input[@id='slider-1']";
	String btnIsEnabledLocator = "//button[@id='button-enabled']";

	String pwdLocator = "//input[@id='password']";
	String radiodisabledLocator = "//input[@id='radio-disabled']";
	String biographyAreaLocator = "//textarea [@id='bio']";
	String jobRole2Locator = ".//select[@id='job2']";
	String slider2Locator = ".//input[@id='slider-2']";
	String btnIsDisableLocator = ".//button[@id='button-disabled']";

	@BeforeTest
	public void initBrowser() {
		driver = new FirefoxDriver();
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test
	public void TC_01_VerifyElementISDisplay() {
		WebElement mailTextbox = driver.findElement(By.xpath(mailTxtLocator));
		WebElement ageRadioButton = driver.findElement(By.xpath(ageRadioLocator));
		WebElement eduTextArea = driver.findElement(By.xpath(eduAreaLocator));
		Assert.assertTrue(mailTextbox.isDisplayed());
		Assert.assertTrue(ageRadioButton.isDisplayed());
		Assert.assertTrue(eduTextArea.isDisplayed());
		sendKey(mailTextbox, "Automation Testing");
		sendKey(eduTextArea, "Automation Testing");
		selectElement(ageRadioButton);
	}

	@Test
	public void TC_02_CheckStatusElement() {
		/* Define element is enable */
		WebElement mailTextbox = driver.findElement(By.xpath(mailTxtLocator));
		WebElement ageRadioButton = driver.findElement(By.xpath(ageRadioLocator));
		WebElement eduTextArea = driver.findElement(By.xpath(eduAreaLocator));
		WebElement jobRole1 = driver.findElement(By.xpath(jobRole1Locator));
		WebElement devChBox = driver.findElement(By.xpath(devChBoxLocator));
		WebElement slider1 = driver.findElement(By.xpath(slider1Locator));
		WebElement btnIsEnabled = driver.findElement(By.xpath(btnIsEnabledLocator));
		/* Define element is disable */
		WebElement pwdTexbox = driver.findElement(By.xpath(pwdLocator));
		WebElement radiodisabled = driver.findElement(By.xpath(radiodisabledLocator));
		WebElement biographyArea = driver.findElement(By.xpath(biographyAreaLocator));
		WebElement jobRole2Dropdown = driver.findElement(By.xpath(jobRole2Locator));
		WebElement slider2 = driver.findElement(By.xpath(slider2Locator));
		WebElement btnIsDisableButton = driver.findElement(By.xpath(btnIsDisableLocator));
		/*
		 * List element is enable
		 */
		WebElement[] elementsEnable = { mailTextbox, ageRadioButton, eduTextArea, jobRole1, devChBox, slider1,
				btnIsEnabled, pwdTexbox };
		List<WebElement> listEleEnable = Arrays.asList(elementsEnable);
		/*
		 * List element is disable
		 */
		WebElement[] elementsDisable = { pwdTexbox, radiodisabled, biographyArea, jobRole2Dropdown, slider2,
				btnIsDisableButton };
		List<WebElement> listEleDisable = Arrays.asList(elementsDisable);

		for (int i = 0; i < listEleEnable.size(); i++) {
			Assert.assertTrue("Element " + listEleEnable.get(i) + " is disable", listEleEnable.get(i).isEnabled());
			System.out.println("Element " + listEleEnable.get(i).getAttribute("name") + " is enable");
		}

		for (int i = 0; i < listEleDisable.size(); i++) {
			Assert.assertFalse(listEleDisable.get(i).isEnabled());
			System.out.println("Element " + listEleEnable.get(i).getAttribute("name") + " is disable");
		}
	}

	@Test
	public void TC_03_ElementIsSelected() {
		WebElement ageRadioButton = driver.findElement(By.xpath(ageRadioLocator));
		WebElement devChBox = driver.findElement(By.xpath(devChBoxLocator));
		List<WebElement> listElement = new ArrayList<>();
		listElement.add(ageRadioButton);
		listElement.add(devChBox);
		int i = 0;
		do {
			selectElement(devChBox);
			selectElement(ageRadioButton);
			i++;
		} while (i < 2);

	}

	public void sendKey(WebElement element, String text) {
		if (element.isEnabled()) {
			element.clear();
			element.sendKeys(text);
		}
	}

	public void selectElement(WebElement e) {
		if (e.isSelected() == false) {
			e.click();
		}
	}

	@AfterTest
	public void cleanData() {
		driver.quit();
	}

}
