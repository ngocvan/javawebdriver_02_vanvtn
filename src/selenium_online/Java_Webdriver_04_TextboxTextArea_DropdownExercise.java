package selenium_online;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Java_Webdriver_04_TextboxTextArea_DropdownExercise {

	WebDriver driver;
	String customerName, dateOfBirth, address, city, state, pin, mobileNumber, email, password, addressEdit, cityEdit;

	@BeforeTest
	public void initBrowser() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		customerName = "Mr Dam";
		dateOfBirth = "16/01/1990";
		address = "HCM";
		city = "HCM";
		state = "hcm";
		pin = "123456";
		mobileNumber = "123434567";
		email = "automation" + randomMailNumber() + "@gmail.com";
		password = "56789";
		addressEdit = "adresss1";
		cityEdit = "Da nang";
	}

	@Test
	public void TC_01_HandleDropdownlist() {
		driver.get("http://daominhdam.890m.com/");
		WebElement dropdownlist = driver.findElement(By.xpath("//select[@id='job1']"));
		Select dropdown = new Select(dropdownlist);
		Assert.assertFalse(dropdown.isMultiple());
		// select Automation Tester by Visible Text
		dropdown.selectByVisibleText("Automation Tester");
		Assert.assertEquals(dropdown.getFirstSelectedOption().getText(), "Automation Tester");
		// select Manual Tester by selectValue
		dropdown.selectByValue("manual");
		Assert.assertEquals(dropdown.getFirstSelectedOption().getText(), "Manual Tester");
		// Select value Mobile Tester in dropdownlist by index
		dropdown.selectByIndex(3);
		Assert.assertEquals(dropdown.getFirstSelectedOption().getText(), "Mobile Tester");
		// Verify that dropdown list have 5 values
		int numberOfValue = dropdown.getOptions().size();
		Assert.assertTrue(numberOfValue == 5);

	}

	@Test
	public void TC_02_HandleTextbox_Textarea() {
		driver.get("http://demo.guru99.com/v4");
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys("mngr107888 ");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("EmEpazy");
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
		WebElement welcomeMessage = driver.findElement(By.xpath("//marquee"));
		Assert.assertTrue(welcomeMessage.isDisplayed());
		Assert.assertEquals(welcomeMessage.getText().trim(), "Welcome To Manager's Page of Guru99 Bank");
		driver.findElement(By.xpath("//a[contains(text(),'New Customer')]")).click();
		// Create new customer

		driver.findElement(By.xpath("//input[@name='name']")).sendKeys(customerName);
		driver.findElement(By.xpath("//input[@id='dob']")).sendKeys(dateOfBirth);
		driver.findElement(By.xpath("//textarea[@name='addr']")).sendKeys(address);
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys(city);
		driver.findElement(By.xpath("//input[@name='state']")).sendKeys(state);
		driver.findElement(By.xpath("//input[@name='pinno']")).sendKeys(pin);
		driver.findElement(By.xpath("//input[@name='telephoneno']")).sendKeys(mobileNumber);
		WebElement mail = driver.findElement(By.xpath("//input[@name='emailid']"));
		mail.clear();
		mail.sendKeys(email);
		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.clear();
		pwd.sendKeys(password);
		driver.findElement(By.xpath("//input[@name='sub']")).click();

		// Verify new customer had been created successfully
		WebElement msg = driver.findElement(By.xpath("//*[@id='customer']//p[@class='heading3']"));
		Assert.assertEquals(msg.getText().trim(), "Customer Registered Successfully!!!");
		String cusId = driver.findElement(By.xpath("//td[contains(text(),'Customer ID')]/following-sibling::td"))
				.getText();
		System.out.println("Customer id: " + cusId);

		// Verify Customer Name, Address
		driver.findElement(By.xpath("//a[contains(text(),'Edit Customer')]")).click();
		WebElement msgedit = driver.findElement(By.xpath("//p[@class='heading3']"));
		Assert.assertEquals(msgedit.getText().trim(), "Edit Customer Form");
		driver.findElement(By.xpath("//input[@name='cusid']")).sendKeys(cusId);
		driver.findElement(By.xpath("//input[@name='AccSubmit']")).click();
		Assert.assertEquals(driver.findElement(By.xpath("//input[@name='name']")).getAttribute("value"), customerName);
		Assert.assertEquals(driver.findElement(By.xpath("//textarea[@name='addr']")).getText().trim(), address);

		// edit
		driver.findElement(By.xpath("//textarea[@name='addr']")).clear();
		driver.findElement(By.xpath("//textarea[@name='addr']")).sendKeys(addressEdit);
		driver.findElement(By.xpath("//input[@name='city']")).clear();
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys(cityEdit);
		driver.findElement(By.xpath("//input[@name='sub']")).click();
		Assert.assertEquals(
				driver.findElement(By.xpath("//td[contains(text(),'Address')]/following-sibling::td")).getText(),
				addressEdit);
		Assert.assertEquals(
				driver.findElement(By.xpath("//td[contains(text(),'City')]/following-sibling::td")).getText(),
				cityEdit);
	}

	public int randomMailNumber() {
		Random rad = new Random();
		int number = rad.nextInt(5000) + 1;
		return number;
	}

	@AfterTest
	public void cleanData() {
		driver.quit();
	}
}
