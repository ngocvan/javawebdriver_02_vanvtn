package selenium_online;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Java_Webdriver_07_IframeWindowPopupExercise {

	WebDriver driver;

	@BeforeMethod
	public void initBrowser() {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		// driver = new ChromeDriver();
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@Test
	public void TC_01_Iframe() {
		driver.get("http://www.hdfcbank.com/");
		// Close popup if it is visible
		try {
			WebElement iframeImage = driver.findElement(By.xpath("//iframe[@id='vizury-notification-template']"));
			driver.switchTo().frame(iframeImage);
			driver.findElement(By.xpath(".//*[@id='div-close']")).click();
		} catch (Exception e) {
			System.out.println("This site does not have iframe with id vizury-notification-template");
		}
		// Verify message text
		WebElement iFrame01 = driver.findElement(By.xpath("//div[@class='flipBannerWrap']//iframe"));
		driver.switchTo().frame(iFrame01);
		WebElement message = driver.findElement(By.xpath("//*[@id='messageText']"));
		Assert.assertTrue(message.isDisplayed());
		driver.switchTo().defaultContent();
		// Verify banner image
		WebElement iframe02 = driver.findElement(By.xpath("//div[@class='slidingbanners']//iframe"));
		driver.switchTo().frame(iframe02);
		List<WebElement> bannerImage = driver.findElements(By.xpath("//*[@class='bannerimage']"));
		Assert.assertEquals(6, bannerImage.size());
		driver.switchTo().defaultContent();
		// Verify flipper banner
		WebElement flipBanner = driver.findElement(By.xpath("//div[@class='flipBanner']"));
		Assert.assertTrue(flipBanner.isDisplayed());
	}

	@Test
	public void TC_02_Window() {
		driver.get("http://daominhdam.890m.com/");
		String currentWindow = driver.getWindowHandle();
		WebElement link = driver.findElement(By.xpath("//a[contains(text(),'Click Here')]"));
		link.click();
		switchToChildWindow(currentWindow);
		String title = driver.getTitle();
		Assert.assertEquals("Google", title);
		driver.close();
	}

	@Test
	public void TC_03() {
		driver.get("http://www.hdfcbank.com/");
		// Close popup if it is visible
		String parentWindow = driver.getWindowHandle();
		try {
			WebElement iframeImage = driver.findElement(By.xpath("//iframe[@id='vizury-notification-template']"));
			driver.switchTo().frame(iframeImage);
			driver.findElement(By.xpath(".//*[@id='div-close']")).click();
		} catch (Exception e) {
			System.out.println("This site does not have iframe with id vizury-notification-template");
		}
		// click agri link
		WebElement agriLink = driver.findElement(By.xpath("//a[text()='Agri']"));
		agriLink.click();
		switchToWindowByTitle("HDFC Bank Kisan Dhan Vikas e-Kendra");
		// click detail account link
		WebElement accountDetail = driver.findElement(By.xpath("//p[contains(text(),'Account Details')]"));
		accountDetail.click();
		switchToWindowByTitle("Welcome to HDFC Bank NetBanking");
		// click Privacy Policy link
		WebElement iFrame03 = driver.findElement(By.xpath("//frame[@name='footer']"));
		driver.switchTo().frame(iFrame03);
		WebElement privacyPolicyLink = driver.findElement(By.xpath("//a[contains(text(),'Privacy Policy')]"));
		privacyPolicyLink.click();
		switchToWindowByTitle(
				"HDFC Bank - Leading Bank in India, Banking Services, Private Banking, Personal Loan, Car Loan");
		WebElement csrLink = driver.findElement(By.xpath("//div[@class='hygeinenav']//li[4]"));
		csrLink.click();
		driver.switchTo().defaultContent();
		closeAllWithoutParentWindows(parentWindow);

	}

	public void switchToChildWindow(String parent) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindow : allWindows) {
			if (!runWindow.equals(parent)) {
				driver.switchTo().window(runWindow);
				break;
			}
		}
	}

	public void switchToWindowByTitle(String title) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindows : allWindows) {
			driver.switchTo().window(runWindows);
			String currentWin = driver.getTitle();
			if (currentWin.equals(title)) {
				break;
			}
		}
	}

	public boolean closeAllWithoutParentWindows(String parentWindow) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindows : allWindows) {
			if (!runWindows.equals(parentWindow)) {
				driver.switchTo().window(runWindows);
				driver.close();
			}
		}
		driver.switchTo().window(parentWindow);
		if (driver.getWindowHandles().size() == 1)
			return true;
		else
			return false;
	}

	@AfterMethod
	public void cleanData() {
		driver.quit();
	}

}
