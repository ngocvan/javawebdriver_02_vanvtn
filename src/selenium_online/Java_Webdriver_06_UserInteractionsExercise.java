package selenium_online;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Java_Webdriver_06_UserInteractionsExercise {
	WebDriver driver;

	@BeforeMethod
	public void initBrowser() {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		// driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@Test
	public void TC_0101_HoverOver() {
		driver.get("http://daominhdam.890m.com/#");
		WebElement tooltip = driver.findElement(By.xpath("//a[contains(text(),'Hover over me')]"));
		Actions action = new Actions(driver);
		action.moveToElement(tooltip).perform();
		WebElement hover = driver.findElement(By.xpath("//div[@class='tooltip-inner']"));
		Assert.assertEquals("Hooray!", hover.getText());
	}

	@Test
	public void TC_0102_HoverOver() {
		driver.get("http://www.myntra.com/");
		WebElement menu = driver.findElement(By.xpath("//span[contains(@class,'sprites-user')]"));
		WebElement login = driver.findElement(By.xpath("//a[contains(text(),'login')]"));
		Actions action = new Actions(driver);
		action.moveToElement(menu).moveToElement(login).click().perform();
		WebElement loginForm = driver.findElement(By.xpath("//*[@id='mountRoot']//div[@class='login-box']"));
		Assert.assertTrue(loginForm.isDisplayed());
	}

	@Test
	public void TC_02_ClickAndHold() {
		driver.get("http://jqueryui.com/resources/demos/selectable/display-grid.html");
		List<WebElement> elements = driver.findElements(By.xpath("//*[@id='selectable']/li"));
		Actions action = new Actions(driver);
		action.clickAndHold(elements.get(0)).clickAndHold(elements.get(3)).click().build().perform();
		List<WebElement> elementSelected = driver
				.findElements(By.xpath("//*[@id='selectable']/li[contains(@class,'ui-selected')]"));
		Assert.assertEquals(4, elementSelected.size());
	}

	@Test
	public void TC_03_DoubleClick() {
		driver.get("http://www.seleniumlearn.com/double-click");
		WebElement elements = driver.findElement(By.xpath("//button[contains(text(),'Double-Click Me')]"));
		Actions action = new Actions(driver);
		action.doubleClick(elements).perform();
		Alert al = driver.switchTo().alert();
		String msg = al.getText();
		Assert.assertEquals("The Button was double-clicked.", msg);
		al.accept();
	}

	@Test
	public void TC_04_RightClick() {
		driver.get("http://swisnl.github.io/jQuery-contextMenu/demo.html");
		WebElement element = driver.findElement(By.xpath("//span[contains(@class,'btn-neutral')]"));
		Actions action = new Actions(driver);
		action.contextClick(element).perform();
		WebElement quit = driver.findElement(By.xpath("//li[contains(@class,'context-menu-icon-quit')]"));
		action.moveToElement(quit).perform();
		WebElement quiltHover = driver
				.findElement(By.xpath("//li[contains(@class,'context-menu-visible') and contains(.,'Quit')]"));
		Assert.assertTrue(quiltHover.isDisplayed());
		quiltHover.click();
		Alert al = driver.switchTo().alert();
		al.accept();
	}

	@Test
	public void TC_0501_DragDrop() {
		driver.get("http://demos.telerik.com/kendo-ui/dragdrop/angular");
		WebElement source = driver.findElement(By.xpath("//*[@id='draggable']"));
		WebElement target = driver.findElement(By.xpath("//*[@id='droptarget']"));
		Actions action = new Actions(driver);
		action.dragAndDrop(source, target).release().perform();
		Assert.assertEquals("You did great!", target.getText());
	}

	@Test
	public void TC_0502_DragDropRectangle() {
		driver.get("http://jqueryui.com/resources/demos/droppable/default.html");
		WebElement source = driver.findElement(By.xpath("//*[@id='draggable']/p"));
		WebElement target = driver.findElement(By.xpath("//*[@id='droppable']"));
		Actions action = new Actions(driver);
		action.dragAndDrop(source, target).release().perform();
		Assert.assertEquals("Dropped!", target.getText());
	}

	@AfterMethod
	public void cleanData() {
		driver.quit();
	}

}
