package selenium_online;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Java_Webdriver_08_JavascriptExecutorExercise {

	WebDriver driver;

	@BeforeMethod
	public void initBrowser() {
		System.setProperty("webdriver.ie.driver", ".\\driver\\IEDriverServer.exe");
		driver = new InternetExplorerDriver();
		// driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@Test
	public void TC_01_Javascript_Excecutor(){
		navigateToUrl("http://live.guru99.com/");
		waitForElementByLocator(By.xpath("//a[contains(text(),'Mobile')]"));
		String domain = (String) executeForBrowserElement(driver, "return document.domain");
		Assert.assertEquals("live.guru99.com", domain);

		String url = (String) executeForBrowserElement(driver, "return document.URL");
		Assert.assertEquals("http://live.guru99.com/", url);

		// Click mobile link
		WebElement mobileLink = driver.findElement(By.xpath("//a[contains(text(),'Mobile')]"));
		executeForWebElementByClick(driver, mobileLink);
		waitForElementByLocator(By.xpath("//a[contains(text(),'Mobile')]"));

		// Click add to card button
		WebElement samSungAddToCard = driver.findElement(By
				.xpath("//h2[a[contains(text(),'Samsung Galaxy')]]//following-sibling::div[@class='actions']/button"));
		executeForWebElementByClick(driver, samSungAddToCard);
		waitForElementByLocator(By.cssSelector("div[class='page-title title-buttons']"));
		String messageText = (String) executeForBrowserElement(driver, "return document.documentElement.innerText;");
		Assert.assertTrue(messageText.contains("Samsung Galaxy was added to your shopping cart."));

		// Open PRIVACY POLICY page
		WebElement privacyLink = driver.findElement(By.xpath("//a[contains(text(),'Privacy Policy')]"));
		executeForWebElementByClick(driver, privacyLink);
		waitForElementByLocator(By.cssSelector("div[class='breadcrumbs']"));
		String privacyTitle = (String) executeForBrowserElement(driver, "return document.title");
		Assert.assertEquals("Privacy Policy", privacyTitle);

		scrollToBottomPage(driver);
		WebElement tableDisplayed = driver.findElement(By.xpath(
				"//th[contains(text(),'WISHLIST_CNT')]/following-sibling::td[contains(text(),'The number of items in your Wishlist')]"));
		Assert.assertTrue(tableDisplayed.isDisplayed());
		navigateToUrl("http://demo.guru99.com/v4/");
		waitForElementByLocator(By.cssSelector("input[name='uid']"));
		String domainV4 = (String) executeForBrowserElement(driver, "return document.URL");
		Assert.assertEquals("http://demo.guru99.com/v4/", domainV4);
	}

	@Test
	public void TC_02_RemoveAttribute(){
		driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_disabled");
		WebElement iframeResult = driver.findElement(By.xpath("//iframe[@id='iframeResult']"));
		driver.switchTo().frame(iframeResult);
		WebElement lastName = driver.findElement(By.xpath("//input[@name='lname']"));
		removeAttributeInDOM(driver, lastName, "disabled");
		lastName.sendKeys("Automation Testing");
		WebElement submitButton = driver.findElement(By.xpath("//input[@value='Submit']"));
		submitButton.click();
		WebElement message = driver.findElement(By.cssSelector("div[class*='w3-large']"));
		Assert.assertTrue(message.getText().contains("Automation Testing"));
	}

	@AfterMethod
	public void cleanData() {
		driver.quit();
	}

	public void highlightElement(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.border='6px groove red'", element);
	}

	public Object executeForBrowserElement(WebDriver driver, String javaSript) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript(javaSript);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object executeForWebElementByClick(WebDriver driver, WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object removeAttributeInDOM(WebDriver driver, WebElement element, String attribute) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].removeAttribute('" + attribute + "');", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object scrollToBottomPage(WebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object navigateToUrl(String url) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.location ='" + url + "'");
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public void waitForElementByLocator(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(by));
	}

}
