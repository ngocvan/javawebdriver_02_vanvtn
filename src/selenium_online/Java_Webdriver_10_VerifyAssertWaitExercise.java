package selenium_online;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.google.common.base.Function;

public class Java_Webdriver_10_VerifyAssertWaitExercise {

	WebDriver driver;
	WebDriverWait wait;

	@BeforeMethod
	public void initBrowser() {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		// driver = new FirefoxDriver();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 30);
	}

	@Test
	public void TC_01_ImplicitWait() {
		driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");
		WebElement startBtn = driver.findElement(By.xpath("//*[@id='start']/button"));
		startBtn.click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Assert.assertEquals("Hello World!", driver.findElement(By.xpath("//*[@id='finish']/h4")).getText());

	}

	@Test
	public void TC_02_ExplicitWait() {
		driver.get(
				"http://demos.telerik.com/aspnet-ajax/ajaxloadingpanel/functionality/explicit-show-hide/defaultcs.aspx");
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ctl00_ContentPlaceholder1_Panel1']")));
		WebElement notificationNotSelectedDate = driver
				.findElement(By.xpath("//*[@id='ctl00_ContentPlaceholder1_Label1']"));
		Assert.assertEquals("No Selected Dates to display.", notificationNotSelectedDate.getText().trim());
		WebElement date = driver.findElement(By.xpath("//a[text()='2']"));
		date.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='raDiv']")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[@class='rcSelected']//a[text()='2']")));
		WebElement dateSelect = driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceholder1_Label1']"));
		Assert.assertEquals("Friday, February 02, 2018", dateSelect.getText().trim());

	}

	@Test
	public void TC_03_FluentWait01() {
		driver.get("https://stuntcoders.com/snippets/javascript-countdown/");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='javascript_countdown_time']")));
		WebElement countDown = driver.findElement(By.xpath("//*[@id='javascript_countdown_time']"));
		new FluentWait<WebElement>(countDown).withTimeout(10, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class).until(new Function<WebElement, Boolean>() {
					public Boolean apply(WebElement el) {
						return el.getText().endsWith("02");
					}
				});
	}

	@Test
	public void TC_04_FluentWait02() {
		driver.get("http://toolsqa.wpengine.com/automation-practice-switch-windows/");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='colorVar']")));
		WebElement colorBtn = driver.findElement(By.xpath(".//*[@id='colorVar']"));
		WebElement clock = driver.findElement(By.xpath(".//*[@id='clock']"));
		scrollToElement(clock);
		new FluentWait<WebElement>(colorBtn).withTimeout(10, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class).until(new Function<WebElement, Boolean>() {
					public Boolean apply(WebElement el) {
						return el.getAttribute("style").equals("color: red;");
					}
				});

		new FluentWait<WebElement>(clock).withTimeout(40, TimeUnit.SECONDS).pollingEvery(10, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class).until(new Function<WebElement, Boolean>() {
					public Boolean apply(WebElement el) {
						return el.getText().trim().equals("Buzz Buzz");
					}
				});
	}

	@AfterMethod
	public void cleanData() {
		driver.quit();
	}

	public Object scrollToElement(WebElement el) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].scrollIntoView(true);", el);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}
}
