package selenium_online;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Java_Webdriver_05_RadiobuttonCheckboxAlertExercise {
	WebDriver driver;

	@BeforeMethod
	public void initBrowser() {
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@Test
	public void TC_01_Handle_Button() {
		driver.get("http://daominhdam.890m.com/#");
		WebElement enabledButton = driver.findElement(By.xpath("//button[@id='button-enabled']"));
		enabledButton.click();
		Assert.assertEquals(driver.getCurrentUrl(), "http://daominhdam.890m.com/#");
		driver.navigate().back();
		// click button by java script
		JavascriptExecutor script = (JavascriptExecutor) driver;
		script.executeScript("arguments[0].click();", driver.findElement(By.xpath("//button[@id='button-enabled']")));
		Assert.assertEquals(driver.getCurrentUrl(), "http://daominhdam.890m.com/#");
	}

	@Test
	public void TC_02_Handle_Checkbox() {
		driver.get("http://demos.telerik.com/kendo-ui/styling/checkboxes");
		WebElement checkBox = driver.findElement(
				By.xpath("//label[contains(text(),'Dual-zone air conditioning')]/preceding-sibling::input"));
		clickElementByJavaScript(checkBox);
		Assert.assertTrue(checkBox.isSelected());
		// deselect check box
		if (checkBox.isSelected()) {
			clickElementByJavaScript(checkBox);
		}
		Assert.assertFalse(checkBox.isSelected());
	}

	@Test
	public void TC_03_Handle_Radiobutton() {
		driver.get("http://demos.telerik.com/kendo-ui/styling/radios");
		WebElement radiobutton = driver
				.findElement(By.xpath("//label[contains(text(),'2.0 Petrol, 147kW')]/preceding-sibling::input"));
		clickElementByJavaScript(radiobutton);
		if (radiobutton.isSelected() == false) {
			clickElementByJavaScript(radiobutton);
		}
	}

	@Test
	public void TC_04_Handle_Alert() {
		driver.get("http://daominhdam.890m.com/#");
		WebElement alertButton = driver.findElement(By.xpath("//button[contains(text(),'Click for JS Alert')]"));
		alertButton.click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
		String msg = driver.findElement(By.xpath("//p[@id='result']")).getText().trim();
		Assert.assertEquals(msg, "You clicked an alert successfully");
	}

	@Test
	public void TC_05_Handle_AlertJSConfirm() {
		driver.get("http://daominhdam.890m.com/#");
		WebElement alertButton = driver.findElement(By.xpath("//button[contains(text(),'Click for JS Confirm')]"));
		alertButton.click();
		Alert alert = driver.switchTo().alert();
		String content = alert.getText();
		Assert.assertEquals(content, "I am a JS Confirm");
		alert.dismiss();
		// Verify message after click cancel button
		String msg = driver.findElement(By.xpath("//p[@id='result']")).getText().trim();
		Assert.assertEquals(msg, "You clicked: Cancel");
	}

	@Test
	public void TC_06_Handle_AlertJSPrompt() {
		driver.get("http://daominhdam.890m.com/#");
		WebElement alertButton = driver.findElement(By.xpath("//button[contains(text(),'Click for JS Prompt')]"));
		alertButton.click();
		Alert alert = driver.switchTo().alert();
		String content = alert.getText();
		Assert.assertEquals(content, "I am a JS prompt");
		String text = "topic 5";
		alert.sendKeys(text);
		alert.accept();
		// Verify message after click cancel button
		String msg = driver.findElement(By.xpath("//p[@id='result']")).getText().trim();
		Assert.assertEquals(msg, "You entered: " + text);
	}

	public Object clickElementByJavaScript(WebElement ele) {
		try {
			JavascriptExecutor script = (JavascriptExecutor) driver;
			return script.executeScript("arguments[0].click()", ele);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}

	}

	@AfterMethod
	public void cleanData() {
		driver.quit();
	}

}
