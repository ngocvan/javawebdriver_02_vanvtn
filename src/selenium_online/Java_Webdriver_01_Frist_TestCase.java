package selenium_online;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Java_Webdriver_01_Frist_TestCase {
	WebDriver driver;

	@BeforeClass
	public void initBrowser() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test
	public void TC_01_CheckEnvironment() {
		driver.get("http://demo.guru99.com/v4/");
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys("mngr107888");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("EmEpazy");
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
		WebElement welcomeMessage = driver.findElement(By.xpath("//marquee"));
		Assert.assertTrue(welcomeMessage.isDisplayed());
		Assert.assertEquals(welcomeMessage.getText().trim(), "Welcome To Manager's Page of Guru99 Bank");
		String homePageURL = driver.getCurrentUrl();
		Assert.assertEquals("http://demo.guru99.com/v4/manager/Managerhomepage.php", homePageURL);
	}
	


	@AfterClass
	public void cleanData() {
		driver.quit();
	}

}
