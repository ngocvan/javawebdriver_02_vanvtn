package selenium_online;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Java_Webdriver_09_UploadFileExercise {

	WebDriver driver;
	String filePath, fileName, subfolderName, email, firstName;

	@BeforeMethod
	public void initBrowser() {
		System.setProperty("webdriver.ie.driver", ".\\driver\\IEDriverServer.exe");
		// System.setProperty("webdriver.chrome.driver",
		// ".\\driver\\chromedriver.exe");
		// driver = new FirefoxDriver();
		// driver = new ChromeDriver();
		driver = new InternetExplorerDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		filePath = System.getProperty("user.dir") + "\\uploads\\panda.jpg";
		fileName = "panda.jpg";
		subfolderName = "New folder" + randomNumber();
		email = "example" + randomNumber() + "@gmail.com";
		firstName = "Auto test";
	}

	@Test
	public void TC_01_UploadFileBySendkey() {
		driver.get("http://www.helloselenium.com/2015/03/how-to-upload-file-using-sendkeys.html");
		WebElement uploadButton = driver.findElement(By.xpath("//input[@name='uploadFileInput']"));
		uploadButton.sendKeys(filePath);
	}

	@Test
	public void TC_02_UploadFileByAutoIt() throws Exception {
		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		driver.findElement(By.cssSelector("span[class*='fileinput-button']")).click();
		Runtime.getRuntime().exec(new String[] { ".\\upload\\ie.exe", filePath });
		Assert.assertTrue(driver.findElement(By.cssSelector("p[class='name']")).isDisplayed());
	}

	@Test
	public void TC_03_UploadFileByRobotClass() throws Exception {
		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		StringSelection location = new StringSelection(filePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(location, null);
		driver.findElement(By.cssSelector("span[class*='fileinput-button']")).click();
		Robot robot = new Robot();
		Thread.sleep(2000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(2000);
		Assert.assertTrue(driver.findElement(By.cssSelector("p[class='name']")).isDisplayed());
	}

	@Test
	public void TC_04_UploadFile() {
		driver.get("https://encodable.com/uploaddemo/");
		WebElement uploadButton = driver.findElement(By.cssSelector("input[id='uploadname1']"));
		uploadButton.sendKeys(filePath);
		WebElement dropdownList = driver.findElement(By.cssSelector("select[name ='subdir1']"));
		Select dropdown = new Select(dropdownList);
		dropdown.selectByVisibleText("/uploaddemo/files/");
		WebElement subfolder = driver.findElement(By.cssSelector("input[id='newsubdir1']"));
		subfolder.sendKeys(subfolderName);
		WebElement mailTxt = driver.findElement(By.cssSelector("input[id='formfield-email_address']"));
		mailTxt.sendKeys(email);
		WebElement firstNameTxt = driver.findElement(By.cssSelector("input[id='formfield-first_name']"));
		firstNameTxt.sendKeys(firstName);
		WebElement beingUploadBtn = driver.findElement(By.cssSelector("input[id='uploadbutton']"));
		beingUploadBtn.click();
		waitForElementByLocator(
				By.xpath("//div[@id='uploadDoneContainer']//dt[contains(text(),'Your upload is complete')]"));

		String mailAddress = driver
				.findElement(By.xpath("//div[@id='uploadDoneContainer']//dd[starts-with(text(),'Email Address')]"))
				.getText();
		Assert.assertTrue(mailAddress.contains(email));

		String firstNameInfo = driver
				.findElement(By.xpath("//div[@id='uploadDoneContainer']//dd[starts-with(text(),'First Name')]"))
				.getText();
		Assert.assertTrue(firstNameInfo.contains(firstName));

		String file = driver
				.findElement(By.xpath("//div[@id='uploadDoneContainer']//dt[starts-with(text(),'File 1 of 1')]"))
				.getText();
		Assert.assertTrue(file.contains(fileName));

		driver.findElement(By.xpath("//a[contains(text(),'View Uploaded Files')]")).click();

		waitForElementByLocator(By.xpath("//a[contains(text(),'uploads')]"));
		driver.findElement(By.xpath("//a[contains(text(),'" + subfolderName + "')]")).click();

		WebElement imageDisplay = driver.findElement(By.xpath("//a[contains(text(),'" + fileName + "')]"));
		Assert.assertTrue(imageDisplay.isDisplayed());

	}

	@AfterMethod
	public void cleanData() {
		driver.quit();
	}

	public int randomNumber() {
		Random rad = new Random();
		int number = rad.nextInt(5000) + 1;
		return number;
	}

	public void waitForElementByLocator(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(by));
	}

}
