package selenium_testng;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestNG_05_MultiBrowsers {

	WebDriver driver;
	WebElement errorMessage;
	int timeOut = 15;

	@Parameters({ "userName", "passWord" })
	@Test
	public void TC_01_Login(String userName, String passWord) {
		driver.get("http://demo.guru99.com/v4/");
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys(userName);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(passWord);
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
		waitForElementByLocator(By.xpath("//marquee"));
		WebElement welcomeMessage = driver.findElement(By.xpath("//marquee"));
		Assert.assertTrue(welcomeMessage.isDisplayed());
		Assert.assertEquals(welcomeMessage.getText().trim(), "Welcome To Manager's Page of Guru99 Bank");
		String homePageURL = driver.getCurrentUrl();
		Assert.assertEquals("http://demo.guru99.com/v4/manager/Managerhomepage.php", homePageURL);
	}

	@Parameters("browserName")
	@BeforeTest
	public void beforeTest(String browserName) {
		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		else if (browserName.equalsIgnoreCase("firefox")) {
			driver = new FirefoxDriver();
		}
		else if (browserName.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", ".\\driver\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

	public void waitForElementByLocator(By by) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		wait.until(ExpectedConditions.elementToBeClickable(by));
	}

}
