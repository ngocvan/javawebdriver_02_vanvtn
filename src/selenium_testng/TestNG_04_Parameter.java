package selenium_testng;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestNG_04_Parameter {

	WebDriver driver;
	WebElement errorMessage;
	int timeOut = 10;

	@Parameters({ "userName", "passWord" })
	@Test
	public void TC_01_Login(String userName, String passWord) {
		driver.get("http://demo.guru99.com/v4/");
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys(userName);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(passWord);
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
		WebElement welcomeMessage = driver.findElement(By.xpath("//marquee"));
		Assert.assertTrue(welcomeMessage.isDisplayed());
		Assert.assertEquals(welcomeMessage.getText().trim(), "Welcome To Manager's Page of Guru99 Bank");
		String homePageURL = driver.getCurrentUrl();
		Assert.assertEquals("http://demo.guru99.com/v4/manager/Managerhomepage.php", homePageURL);
	}

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
