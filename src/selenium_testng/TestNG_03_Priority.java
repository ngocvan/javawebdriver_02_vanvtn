package selenium_testng;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestNG_03_Priority {

	WebDriver driver;
	WebElement errorMessage;
	int timeOut = 10;

	@Test(groups = { "verify" }, enabled = false)
	public void TC_01_LoginEmpty() {
		driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("");
		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("");
		driver.findElement(By.id("send2")).click();
		errorMessage = driver.findElement(By.id("advice-required-entry-email"));
		Assert.assertTrue(errorMessage.isDisplayed());
		Assert.assertEquals(errorMessage.getText().trim(), "This is a required field.");
	}

	@Test(groups = { "verify" })
	public void TC_02_LoginWithEmailInvalid() {
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("123434234@12312.123123");
		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("");
		driver.findElement(By.id("send2")).click();
		errorMessage = driver.findElement(By.id("advice-validate-email-email"));
		Assert.assertTrue(errorMessage.isDisplayed());
		Assert.assertEquals(errorMessage.getText().trim(),
				"Please enter a valid email address. For example johndoe@domain.com.");
	}

	@Test(groups = { "verify" })
	public void TC_03_LoginWithPasswordIncorrect() {
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("automation@gmail.com");
		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("123");
		driver.findElement(By.id("send2")).click();
		errorMessage = driver.findElement(By.id("advice-validate-password-pass"));
		Assert.assertTrue(errorMessage.isDisplayed());
		Assert.assertEquals(errorMessage.getText().trim(),
				"Please enter 6 or more characters without leading or trailing spaces.");
	}

	@Test(groups = { "createAccount" })
	public void TC_04_CreateNewAccount() {
		driver.findElement(By.xpath("//a[contains(@title,'Create an Account')]")).click();
		driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("Automation");
		driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("Testing");
		driver.findElement(By.xpath("//input[@id='email_address']"))
				.sendKeys("demo" + randomMailNumber() + "@gmail.com");
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//input[@id='confirmation']")).sendKeys("123456");
		driver.findElement(By.xpath("//button[@title='Register']")).click();
		WebElement successMessage = driver.findElement(By.xpath("//div[@class='dashboard']//span"));
		Assert.assertTrue(successMessage.isDisplayed());
		Assert.assertEquals(successMessage.getText().trim(), "Thank you for registering with Main Website Store.");

	}

	public int randomMailNumber() {
		Random rad = new Random();
		int number = rad.nextInt(5000) + 1;
		return number;
	}

	@BeforeMethod(groups = { "verify" })
	public void beforeMethod() {
		driver.get("http://live.guru99.com/");
		driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("afterMethod");
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("beforeClass");
	}

	@AfterClass
	public void afterClass() {
		System.out.println("afterClass");
	}

	@BeforeTest(groups = { "verify" })
	public void beforeTest() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@AfterTest (groups = { "verify" })
	public void afterTest() {
		driver.quit();
	}

	@BeforeSuite 
	public void beforeSuite() {
		System.out.println("beforeSuite");
	}

	@AfterSuite
	public void afterSuite() {
		System.out.println("afterSuite");
	}

}
